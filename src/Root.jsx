import { h } from 'preact'
import {Switch, Route} from 'wouter-preact'
import GameBoard from './components/GameBoard/GameBoard'

const Root = () => {
    return (
        <div>
            <Switch>
                <Route path="/:encodedGameBoard?" >
                    {params => <GameBoard encodedBoard={params.encodedGameBoard} /> }
                </Route>
            </Switch>
        </div>
    )
}

export default Root
