import { h } from 'preact'
import { useEffect, useState } from 'preact/hooks'
import styles from './style.css'
import LogicInstance from '../../comlink/Logic'
import { useLocation } from 'wouter-preact'

const GameBoard = ({ encodedBoard }) => {
    const [, setLocation] = useLocation()
    const [board, setBoard] = useState([])
    const [playerNum, setPlayerNum] = useState(0)

    useEffect(async () => {
        if (encodedBoard) {
            const instance = await LogicInstance()
            const gameBoard = await instance.populateFromEncoded(encodedBoard)
            setBoard(mapInitialGameboard(gameBoard))
        }
    }, [])

    const selectPlayer = (playerNum) => {
        setPlayerNum(playerNum)
    }

    const newBoard = async () => {
        setPlayerNum(0)

        const instance = await LogicInstance()

        const board = await instance.populateBoard()

        setBoard(mapInitialGameboard(board))

        setLocation(`/${await instance.lastGeneratedEncoding}`)
    }

    const copyLinkToClipboard = () => {
        navigator.clipboard.writeText(window.location)
            .catch((e) => { throw e })
    }

    const mapInitialGameboard = (gameboard) => {
        return gameboard.map((row) => {
            return row.map((column) => ({
                ...column,
                selected: false
            }))
        })
    }

    const setSelectedState = (row, column, value = true) => {
        const newBoard = [ ...board ]
        newBoard[row][column].selected = value
        setBoard(newBoard)
    }

    const Rows = () => {
        if (board.length === 0) {
            return []
        }
        return board.map((row, rowIndex) => row.map((column, columnIndex) => {
            const params = {
                classNames: undefined,
            }

            if ((column.id === 1 || column.id === 3) && playerNum === 1) {
                params.classNames = `${styles.greenCard}`
            } else if ((column.id === 2 || column.id === 3) && playerNum === 2) {
                params.classNames = `${styles.blueCard}`
            } else if ((column.id === 4 && playerNum > 0 ) || (column.id === 5 && playerNum === 1) || (column.id === 6 && playerNum === 2)) {
                params.classNames = `${styles.assassinCard}`
            } else {
                params.classNames = `${styles.normalCard}`
            }

            if (column.search) { params.classNames += ` ${styles.selected}` }

            return (
                <div
                    className={`${styles.gamePiece} ${params.classNames}`}
                    onClick={() => setSelectedState(rowIndex, columnIndex, column.search)}
                >
                    {column.word}
                </div>
            )
        }))
    }


    return (
        <div>
            <div className={styles.playerSelect}>
                <input type="button" value="New Board" onClick={newBoard} className={styles.button} />
                <input type="button" value="Send this link to a friend" onClick={copyLinkToClipboard} className={styles.button} />
            </div>
            <div className={styles.playerSelect}>
                <input
                    type="button"
                    value="Codemaster 1"
                    onClick={() => selectPlayer(1)}
                    className={`${styles.button} ${styles.greenCard}`}
                />
                <input
                    type="button"
                    value="Codemaster 2"
                    onClick={() => selectPlayer(2)}
                    className={`${styles.button} ${styles.blueCard}`}
                />
                <input
                    type="button"
                    value="Teammate"
                    onClick={() => selectPlayer(0)}
                    className={`${styles.button}`}
                />
            </div>
            <div className={styles.container}>
                <Rows />
            </div>
        </div>
    )
}

export default GameBoard
