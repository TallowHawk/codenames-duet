export const teams = {
    unknown: {
        name: 'Unknown',
        id: 0
    },
    green: {
        name: 'Green',
        id: 1
    },
    blue: {
        name: 'Blue',
        id: 2
    },
    shared: {
        name: 'Shared',
        id: 3
    },
    sharedAssassin: {
        name: 'Shared Assassin',
        id: 4
    },
    greenAssassin: {
        name: 'Green Assassin',
        id: 5
    },
    blueAssassin: {
        name: 'Blue Assassin',
        id: 6
    },
}

export const teamsIds = (() =>
        Object.entries(teams)
            .reduce((acc, [key, value]) => {
                acc[value.id] = value
                return acc
            }, {})
)()
