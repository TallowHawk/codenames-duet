import * as Comlink from 'comlink'
import { teams, teamsIds } from './teamConstants'
import words from './words.json'
import {decode, encode} from './encoding'

class Logic {
    lastGeneratedEncoding = null

    populateBoard = () => {
        const boardSeed = Array(25).fill(teams.unknown.id)
        const firstTeam = this._getRandomInt(1) + 1

        this._generateType(boardSeed, 6, firstTeam)

        this._generateType(boardSeed, 6, firstTeam === teams.green.id ? teams.blue.id : teams.green.id)

        this._generateType(boardSeed, 3, teams.shared.id)

        this._generateType(boardSeed, 1, teams.sharedAssassin.id)
        this._generateType(boardSeed, 1, teams.greenAssassin.id)
        this._generateType(boardSeed, 1, teams.blueAssassin.id)

        const wordIndexes = this._generateUnique(words.length, 25)

        this.lastGeneratedEncoding = encode(boardSeed, wordIndexes)

        return this._createBoardMatrix(boardSeed, wordIndexes)
    }

    populateFromEncoded = (encoded) => {
        try {
            const [boardSeed, wordIndexes] = decode(encoded)

            return this._createBoardMatrix(boardSeed, wordIndexes)
        } catch (e) {
            console.error('Unable to decode')
            return []
        }
    }

    _createBoardMatrix = (boardSeed, wordIndexes) => {
        const board = Array(5)
        for (let x = 0; x < 5; x += 1) {
            board[x] = []
            for (let y = 0; y < 5; y += 1) {
                const pos = x * 5 + y
                const obj = teamsIds[boardSeed[pos]]
                const newObj = { ...obj, word: words[wordIndexes[pos]] }
                board[x].push(newObj)
            }
        }
        return board
    }

    _getRandomInt = (max) => Math.floor(Math.random() * Math.floor(max))

    _generateUnique = (max, n) => {
        const nums = new Set()
        while (nums.size < n) {
            nums.add(this._getRandomInt(max))
        }

        return [...nums]
    }

    _generateType = (typeArray, numberOfTypes, value) => {
        const lookupObject = this._mapLookupArray(typeArray)
        const positionsArray = this._generateUnique(lookupObject.length, numberOfTypes)
        this._mapArrayPosViaLookupArray(typeArray, positionsArray, lookupObject, value)
    }

    _mapArrayPosViaLookupArray = (array, positionsArray, LookupArray, value) => {
        positionsArray.forEach((position) => {
            const correctPos = LookupArray[position]
            array[correctPos] = value
        })
    }

    _mapLookupArray = (array) =>
        array.reduce((acc, cur, index) => {
            if (cur === 0) {
                acc.push(index)
            }
            return acc
        }, [])
}

Comlink.expose(Logic)
