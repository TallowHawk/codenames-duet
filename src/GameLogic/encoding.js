export const encode = (boardSeed, wordIndexes) => {
    const encodedBoardSeed = encodeBoardSeed(boardSeed)
    const encodedWordIndexes = encodeWordIndexes(wordIndexes)
    return `${encodedBoardSeed}|${encodedWordIndexes}`
}

const encodeBoardSeed = (boardSeed) => {
    let encoded = boardSeed
        .reduce((acc, value) => {
            if (acc.length === 0 ) {
                acc.push(value === 0 ? '$' : value)
                return acc
            }

            const lastIndex = acc.length - 1
            const last = acc[lastIndex]

            if (last === '$' && value === 0) {
                acc.push('$')
            } else if (last === '$') {
                acc.push(value)
            } else {
                acc[lastIndex] = `${last}${value}`
            }
            return acc
        }, [])

    encoded[encoded.length - 1] = BigInt(encoded[encoded.length - 1]).toString(36)

    return encoded.join('')
}

const encodeWordIndexes = (wordIndex) => {
    const stringifiedWordIndexes = JSON.stringify(wordIndex)
    return stringifiedWordIndexes.substring(1, stringifiedWordIndexes.length - 1)
}

export const decode = (encoded) => {
    const encodedParts = encoded.split('|')
    return [
        decodeBoardSeed(encodedParts[0]),
        decodeWordIndexes(encodedParts[1])
    ]
}

const decodeWordIndexes = (wordIndexes) => JSON.parse(
    `[${wordIndexes}]`
)


const decodeBoardSeed = (encodedBoardSeed) => {
    const numberIndex = encodedBoardSeed.split('').findIndex((value) => value !== '$')

    let base10String = ''
    let seedArr = []

    if (numberIndex === 0) {
        base10String = BigIntWithRadix(encodedBoardSeed, 36).toString(10)
    } else {
        const base36String = encodedBoardSeed.substring(numberIndex)
        base10String = BigIntWithRadix(base36String, 36).toString(10)

        for (let i = 0; i < numberIndex; i += 1) {
            seedArr.push(0)
        }
    }

    let base10StringArr = base10String.split('')

    for (let i = 0; i < base10StringArr.length; i += 1) {
        seedArr.push(Number.parseInt(base10StringArr[i]))
    }

    return seedArr
}

const BigIntWithRadix = (value, radix) => {
    return [...value.toString()]
        .reduce((r, v) => r * BigInt(radix) + BigInt(parseInt(v, radix)), 0n);
}

