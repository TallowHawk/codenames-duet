import { h, render } from 'preact'
import Root from './Root'

render(h(Root), document.getElementById('root'))
