import * as Comlink from 'comlink'

const Logic = Comlink.wrap(
    new Worker('../GameLogic/Logic', { type: 'module' })
)

let instance = null

export default async () => {
    if (instance === null) {
        instance = await new Logic()
    }
    return instance
}
